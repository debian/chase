#! /bin/sh

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="chase"

(test -f "$srcdir/configure.ac" \
  && test -f "$srcdir/chase.c") || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}


echo "Building macros."
aclocal $ACLOCAL_FLAGS

echo "Building config header."
autoheader

echo "Building makefiles."
automake --add-missing

echo "Building configure."
autoconf

rm -f config.cache

if test -z "$AUTOGEN_SUBDIR_MODE"; then

    echo "Running configure."
    ./configure --enable-maintainer-mode "$@"
    echo
    echo 'run "make"'
    echo
else
    echo
    echo 'run "./configure ; make"'
    echo
fi

