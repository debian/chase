dnl Process this file with autoconf to produce a configure script.
dnl
dnl Copyright (C) 1998-2000 Antti-Juhani Kaijanaho <gaia@iki.fi>
dnl Copyright (C) 2002-2003 Andreas Rottmann <a.rottmann@gmx.at>
dnl
dnl This is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2, or (at your option)
dnl any later version.
dnl 
dnl This software is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this package; see the file COPYING.  If not, write to
dnl the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
dnl Boston, MA 02111-1307, USA.  
dnl

AC_INIT(chase.c)

VERSION="0.5.2"
AC_DEFINE_UNQUOTED(VERSION, "$VERSION", [Program Version])
AC_SUBST(VERSION)

AC_DEFINE(COPYRIGHT_YEAR, ["1998-2004"], [Years of Copyright])

AM_INIT_AUTOMAKE(chase, $VERSION)
AM_CONFIG_HEADER(config.h)

dnl Determine if maintainer portions of the Makefiles should be included.
AM_MAINTAINER_MODE

AC_SUBST(VERSION)

ISODATE=`date +%Y-%m-%d`
AC_SUBST(ISODATE)

dnl Checks for programs.
AC_PROG_CC
if test "$GCC" = yes; then
  CFLAGS="$CFLAGS -Wall -Wmissing-prototypes"
fi
AC_PROG_INSTALL

dnl
dnl Checks for arguments
dnl

dnl copying
copy_location=\"COPYING\"
AC_ARG_ENABLE(copying, AC_HELP_STRING(
 		[--enable-copying=FNAME],
		[tell the program that the file COPYING is FNAME]), [
 if test "$enableval" = no
 then
   copy_location=\"COPYING\"
 else
   copy_location=\"$enableval\"
 fi
])

AC_DEFINE_UNQUOTED(COPYING_LOCATION, $copy_location, [Location of the GNU GPL])

dnl debug
ndebug=1
AC_ARG_ENABLE(debug,   AC_HELP_STRING([--enable-debug], 
				      [compile in noisy diagnostics]),[
  if test "$enableval" = no
  then
    ndebug=1
  else
    ndebug=0
  fi
],ndebug=1)

AC_DEFINE_UNQUOTED(NDEBUG, $ndebug, [Set to 1 to disable noisy debugging output])

dnl Boehm's garbage collector
LIBGC_LIBS=""
LIBGC_CFLAGS=""

AC_MSG_CHECKING([for Boehm's GC headers])
dir=""
for basedir in /usr/local /usr; do
  for x in "" /gc; do
    dir=$basedir/include$x
    if test -f $dir/gc.h; then break; fi;
    dir=""
  done
done
if test -n "$dir"; then
  AC_MSG_RESULT($dir)
  LIBGC_CFLAGS="-I$dir"
  AC_CHECK_LIB(gc, GC_malloc_atomic, [LIBGC_LIBS="-lgc"])
fi
if test -z "LIBGC_LIBS"; then
  AC_MSG_ERROR([You need Boehm's garbage collector.])
fi

AC_SUBST(LIBGC_CFLAGS)
AC_SUBST(LIBGC_LIBS)

dnl Checks for compiler characteristics.
AC_C_CONST
AC_C_INLINE


dnl Checks for functions.
AC_CHECK_FUNCS(chdir fclose fgets fopen fprintf getcwd getopt_long printf)
AC_CHECK_FUNCS(puts readlink strcmp strlen strrchr strstr memmove)

dnl Checks for headers.
AC_CHECK_HEADERS(assert.h errno.h getopt.h stdio.h stdlib.h string.h unistd.h)

AC_CONFIG_FILES([
Makefile
])

AC_OUTPUT