/* chase - chase a symbolic link

   Copyright (C) 1998 Antti-Juhani Kaijanaho

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA

   The author can be reached via mail at (ISO 8859-1 charset for the city)
      Antti-Juhani Kaijanaho
      Helvintie 2 e as 9
      FIN-40500 JYV�SKYL�
      FINLAND
      EUROPE
   and via electronic mail from
      gaia@iki.fi
   If you have a choice, use the email address; it is more likely to
   stay current.

*/

/* $Id: fname.h,v 1.2 1998/12/09 18:20:13 ajk Exp $ */

#ifndef FNAME_H__
#define FNAME_H__

/* Return a fresh string containing the name of the current work
   directory.  */
char *
gnu_getcwd (void);

/* Return a fresh string containing the parent directory part of the
   file name.  */
char *
fname_dir (const char *fname);

/* Return a fresh string containing all but the parent directory part
   of the file name.  */
char *
fname_chopdir (const char *fname);

/* Return a fresh string containing an absolute path to fname.  fname
   is assumed NOT to be a symlink, so optimizations ("./" -> "") are
   made.  Tilde expansion is not made.  */
char *
fname_absolutify (const char * fname);

#endif /* FNAME_H__ */
