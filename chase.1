.TH CHASE 1 "2000-01-02" "Chase 0.5" "Chase user's manual"
\" Copyright (C) 1998-2000 Antti-Juhani Kaijanaho  <gaia@iki.fi>
\" 
\" Permission is granted to make and distribute verbatim copies of
\" this manual provided the copyright notice and this permission notice
\" are preserved on all copies.
\" 
\" Permission is granted to copy and distribute modified versions of this
\" manual under the conditions for verbatim copying, provided that the
\" entire resulting derived work is distributed under the terms of a
\" permission notice identical to this one.
\" 
\" Permission is granted to copy and distribute translations of this
\" manual into another language, under the above conditions for modified
\" versions, except that this permission notice may be included in
\" translations approved by the authors of this manual instead of in
\" the original English.
\" 
.SH NAME
chase \- chase symbolic links
.SH SYNOPSIS
.B chase
[
.B --verbose 
|
.B --from-file
| 
.B -f
|
.B --null
|
.B -0
|
.B --loop-warn-threshold 
COUNT |
.B -w
COUNT |
.B --loop-fail-threshold
COUNT |
.B -l
COUNT |
.B --disable-loop-detection
|
.B -D
]
.IR "" [ file ...]
.sp
.B chase -v
|
.B --version
.sp
.B chase -h
|
.B --help
.SH DESCRIPTION
.B Chase
is a small utility that tracks down the name of the actual file that
is truly accessed, when one hits a symbolic link.  It chases the link,
so to speak.
.PP
In normal operation, you give
.B chase
a name of an existing file.  The program will then show you (or, more
accurately, print to the standard output stream) the name of the real
file that the original file referred to.  It does not matter, if you give
the program the name of a regular file;
.B  chase
will just give you the same name back.  You can give the utility the option
.BR --verbose ,
in order to have it be more verbose in its operation.  You can also
give the program a list of files from which it will read the file
names to be chased (see the option
.BR --from-file ).
.PP
I'll give you a good example of a case where
.B chase
is useful.  In a Debian GNU/Linux system, software installation is
handled by the program
.BR dpkg (8).
There is a way to query
.BR dpkg (8),
which package a file belongs to.  Unfortunately, this does not not
grok all symlinks.  This is a problem, since the Debian system uses
symbolic links extensively for handling several flavours of a program
(say, XEmacs and GNU Emacs and the versions 19 and 20 of both).  Now
with
.BR chase,
one can determine the real file easily and then using this result
as an argument to
.BR dpkg (8).
Observe:
.nf
.sp
ajk@ugh:~$ dpkg -S `which xemacs20`
dpkg: /usr/bin/xemacs20 not found.
ajk@ugh:~$ dpkg -S `which xemacs20 | xargs chase`
xemacs20-nomule: /usr/bin/xemacs-20.4-nomule
ajk@ugh:~$ 
.fi
.PP
When given multiple file names as arguments,
.B chase
will chase every one of them in order.
.SH OPTIONS
The following options are recognized by
.BR chase :
.IP --verbose
Chat about what is being done.
.IP "-f, --from-file"
Treat the file names on the command line as sources for names to be
chased.  If this option is present,
.B chase
will read in each of the files given on the command line (both a sole
dash "\-" and the absence of file names will be taken as meaning the
standard input stream).  Each line in the files is taken as a verbatim
file name that will be chased like it had been given on the command
line of a run without this option.  There is no means for quoting the
newline character; therefore only file names that do not contain
newlines can be specified via the files.  This restriction is lifted
by the -0 option, though.  However, literal spaces are preserved and
are not interpreted as special.
.IP "-0, --null"
This option implies the option -f, which reads file names from a file.
The -0 option modifies the behviour -f so that instead of treating
lines in the file as file names, the file names are expected to be
separated by null characters.  This allows for specifying file names
with newlines in them with -f.
.IP "-w WCOUNT, --loop-warn-threshold=WCOUNT"
.IP "-l LCOUNT, --loop-fail-threshold=LCOUNT"
Set the threshold for warning about a possible symlink loop (WCOUNT)
and for failing because of one (LCOUNT).  There are default values,
which you can find out by invoking
.B chase
with the argument --help.  Using zero as WCOUNT or LCOUNT disables the
check.
.IP "-D, --disable-loop-detection"
Normally
.B chase
keeps a record of all symlinks it has visited when chasing the current
file.  It uses this record to reliably detect symlink loops.  However,
checking the record slows the process a little (more for long chains
of symlinks) and the record requires a small amount of memory (more
for long chains of symlinks), so it may be desirable in some cases to
inhibit this check.  This option disables this loop detection and the
associated recording of visited symlinks.  When this option is in
effect (assuming no bugs in the related code)
.B chase
runs in constant space and linear time relative to the length of the
symlink chains encountered.
.IP "-h, --help"
Output a usage summary and exit successfully.
.IP "-v, --version"
Show version information and exit successfully.
.SH DIAGNOSTICS
The exit status is 0 if and only if all files given in the command
lines exist and none of them are dangling symlinks.
.PP
The following messages are emitted by
.B chase
when it suspects or detects a symlink loop:
.IP "quite many symlink hops, hope we're not looping..."
This means that the chain of symlinks is longer than a given threshold.
This may mean that the chain is infinite (and thus contains a loop
somewhere).  The threshold can be specificed by using the
--loop-warn-threshold command line option.
.IP "too many symlink hops, giving up..."
This means that the chain of symlinks is longer than a given threshold.
This usually means that the chain is infinite (and thus contains a
loop somewhere).  The threshold can be specificed by using the
--loop-fail-threshold command line option.  If you see this message, it
means that
.B chase
has given up on that file.
.IP "symlink loop detected, giving up..."
The symlink chain is looping and therefore there is no result for the
chase.  
.B Chase
has given up on that file.
.PP
Additionally, 
.B Chase
emits several error messages under problemous conditions.  They
all come from the system libraries, so the program has no control over
the actual wording of the messages.  They all follow the format
"program name: file name: error message", where file name is the name
of the offending file or directory.
.SH BUGS
.B Chase
may have bugs. If you find them, please report them to
<rotty@debian.org> or preferably via the Debian bugtracking system.

.SH AUTHOR
.B Chase
has been written by Antti-Juhani Kaijanaho <gaia@iki.fi> and is currently
maintained by Andreas Rottmann <rotty@debian.org>.
.SH "SEE ALSO"
.BR find (1),
.BR ln (1),
.BR namei (1),
.BR readlink (1),
.BR readlink (2),
.BR symlinks (1)
