/* chase - chase a symbolic link

   Copyright (C) 1998, 2000 Antti-Juhani Kaijanaho

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA

   The author can be reached via mail at (ISO 8859-1 charset for the city)
      Antti-Juhani Kaijanaho
      Helvintie 2 e as 9
      FIN-40500 JYV�SKYL�
      FINLAND
      EUROPE
   and via electronic mail from
      gaia@iki.fi
   If you have a choice, use the email address; it is more likely to
   stay current.

*/

/* $Id: gcollect.h,v 1.2 1999/02/03 18:13:51 ajk Exp $ */

#ifndef GCOLLECT_H__
#define GCOLLECT_H__

#include <assert.h>
#include <gc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _(String) (String)
#define N_(String) (String)
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)

/* The following gc-x* functions are our interface to the garbage
   collector.  They are interface-compatible with the routines
   x((re|m)alloc|strdup) in Lars Wirzenius' publib.  NOTE: gc_xmalloc
   assumes that the data in the memory area it allocates does not
   include pointers. */
inline static void *
gc_xmalloc (size_t size)
{
  void * rv;

  rv = GC_malloc_atomic (size);
  if (rv == 0)
    {
      fputs (_ ("Cannot allocate memory."),stderr);
      exit (EXIT_FAILURE);
    }
  return rv;
}

/* This is like gc_xmalloc except that the allocated memory block is
   allowed to contain pointers.  */
inline static void *
gc_xmalloc_container (size_t size)
{
  void * rv;

  rv = GC_malloc (size);
  if (rv == 0)
    {
      fputs (_ ("Cannot allocate memory."),stderr);
      exit (EXIT_FAILURE);
    }
  return rv;
}

inline static void *
gc_xrealloc (void * ptr, size_t size)
{
  void * rv;

  rv = GC_realloc (ptr, size);
  if (rv == 0)
    {
      fputs (_ ("Cannot allocate memory."), stderr);
      exit (EXIT_FAILURE);
    }
  return rv;
}

inline static char *
gc_xstrdup (const char * str)
{
  char * rv;

  assert (str != 0);

  rv = gc_xmalloc (strlen (str) + 1);
  strcpy (rv, str);
  return rv;
}

#endif /* GCOLLECT_H__ */
